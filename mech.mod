<?xml version="1.0" encoding="UTF-8"?>
     <ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <UiMod name="Mech" version="1.83" date="14/10/2008" >
          <Author name="Klesp" email="na@na.com" />
          <Description text="Career resource tool for classes that use it." />
          <Files>
               <File name="locals.lua" />
               <File name="locals/deDE.lua" />
               <File name="locals/enGB.lua" />
               <File name="locals/frFR.lua" />
               <File name="mech.lua" />
               <File name="mech.xml" />
          </Files>
          <OnInitialize>
               <CallFunction name="mech.init" />
          </OnInitialize>
          <OnShutdown>
               <CallFunction name="mech.shutdown" />
          </OnShutdown>
          <SavedVariables>
               <SavedVariable name="mechs" />
          </SavedVariables>
     </UiMod>
</ModuleFile>
