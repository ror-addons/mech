FEATURES
---------------------------------------
Mech is a career resource mechanic addon that will display your current career resource in a new window which is registered to the Warhammer layout editor, therefore you can move it around at your will. This addon also features Warhammer Scrolling Combat Text integration if WSCT is installed.


KNOWN ISSUES
---------------------------------------
None that I'm aware of.


TODO LIST
---------------------------------------
Energy recharge timers (waiting on API fixes)
Resource decay timers (waiting on API fixes)
Word colouring in chat window (waiting on API fixes)
Full XML / GUI interface (waiting on API fixes)
Translations for locals


CHANGES
---------------------------------------
v1.83
---------------------------------------
Tagged as 1.83, full release, minor patch
Added Swordmaster support
Increased length of Mech window due to Swordmaster support

v1.82
---------------------------------------
Tagged as 1.82, full release, minor patch
Small change to the welcome message
Added support for the Disciple of Khaine (sorry, forgot about you guys!)

v1.81
---------------------------------------
Tagged as 1.81, full release, minor patch
Fix for the localisation not loading properly
Fixed a problem with command printout

v1.80
---------------------------------------
Tagged as version 1.80, full release
Fixed a problem that was causing the Black Orc's messages to be cut short
Fixed a potential bug with local messaging function
Localisation is up and running, but I need translations so only English is available.
Fixed the annoying problem of career resources not updating when leaving a Scenario due to a game bug
Fixed an issue that was causing the Mech window to not be gray if you reloaded the UI with 0 resources
Various code clean ups and rewrites

v1.74
---------------------------------------
Tagged as version 1.74, beta release
Added Witch Hunter support
Added Witch Elf support
Made a new file called "! help.txt" which contains info about the slash commands

v1.73
---------------------------------------
Tagged as version 1.73, beta release
Changed "cotf" setting to be defaulted to "on" when first installing
Added cotf options to commands prinout for /mech
Fixed a problem with the foz option causing it to add another line to WSCT
Removed EA Career Resources Window dependancy

v1.72
---------------------------------------
Tagged as version 1.72, beta release
Few fixes implemented for 1.02 functionality
Added "sounds" and "textures" folders for placeholder in future development
Added sounds array and textures array for future development
Adjusted description of Mech in the mod file
Added localisation placeholders whilst I work on those
Removed the config arrays from the core file and created a seperate config file
Duplicated the config file and called it config.bak in case you edit it
Added a chat window print out for when you reset the options.
Large code re-write to several functions
Fixed a lot of problems with the slash handler and re-wrote most of it
Added the option "foz" which allows the user to fade/unfade the resource when it's zero.
Changed 0 (zero) colour for all cirumstances to dark gray for when "foz" is on.
Fixed a problem with the font system that would cause an interface error
Removed a dev/debug line that was causing a script error
Added code that means all your saved options aren't reset when using a newer version
Merged colour1 and colour2 code into one function
Extended the size of the Mech window for Black Orc purposes
Fixed a big bug that caused mechs to fail when first installed until the ui or game was reloaded
Fixed a large bug that stopped the class colours from being anything other than white until mech was reset
Foz options when setting from /mech will now update the mechwin without having to alter resources first
Fixed a problem that caused the mechwin to be blank after a UI reload until resources changed
Added a new "cotf" option, which ignores saved colours and overwrites them with defaults always
Fixed a settings issue that was caused by Mech being loaded before WSCT

v1.70 - v1.71
---------------------------------------
Internal only

v1.60
---------------------------------------
Renamed to 1.60, full release, removed beta status
Fixed a rather annoying spam issue if you somehow got wscti on, but didn't have wsct installed.
Fixed the chat message commands list having out of date options
Corrected some typos in readme.txt
Fixed Ironbreaker not working proper due to typo in code
Corrected a wstring error in update code

v1.51b
---------------------------------------
Renamed to 1.51b, beta release
Added Mech support for the Black Orc
Added Mech support for the Ironbreaker
Added WSCTI support for IB and Black Orc

v1.50b
---------------------------------------
Renamed to 1.50b, beta release
Added WSCT integration
Added new slash commands for WSCT integration
Cleaned up some bad slash commands
Fixed a problem causing the WSCT integration to spam
Added proper Warrior Priest support
Adjusted saved variables to allow for WSCTI
Fixed the date in .mod file from September

v1.40
---------------------------------------
Initial public release.


EXTERNAL LINKS
---------------------------------------
Mech: http://my.curse.com/downloads/war-addons/details/mech.aspx
Libslash: http://war.curse.com/downloads/war-addons/details/libslash.aspx
WSCT: http://war.curse.com/downloads/war-addons/details/wsct.aspx

This mod features LibSlash integration, which is a slash command addon written by Aiiane.
This mod features WSCT integration, which is an addon written by Grayhoof.