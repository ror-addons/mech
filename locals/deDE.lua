if ( mechGameLang == "deDE" ) then

	--[[

	Mech Localisation
	---------------------------------
	Lang:	German
	Author: 	Klesp
	Updated:   14/10/08
	---------------------------------

	]]--
	
	MECH_WELCOME_MSG = "loaded. Use /mech for options (LibSlash req.)";
	MECH_DONE = "Done.";
	MECH_DONT_UNDERSTAND = "You haven't given me a value I understand.";
	MECH_STATE_ALREADY_ON = "I'm already running.";
	MECH_STATE_ON = "I'm now running, and will update when your resources do.";
	MECH_STATE_ALREADY_OFF = "I'm already off.";
	MECH_STATE_OFF = "I've stopped, and will update when your resources do.";
	MECH_COLOURS_COTF_ON = "You can't change colours when you are colouring on the fly. To turn COTF off, use /mech cotf off";
	MECH_COLOURS_COLOUR_INVALID = "Invalid colours, nothing changed.";
	MECH_COLOURS_NUMBER_INVALID = "Invalid number(s), 0 - 255 only.";
	MECH_COLOUR_NEED_THREE = "I need three numeric colours.";
	MECH_OPTIONS_RESET = "My options have been reset to defaults.";
	MECH_FONT_INVALID = "Unknown / invalid font.";
	MECH_FONT_CHANGED = "Font changed to: ";
	MECH_COTF_ALREADY_ON = "I'm already colouring on the fly.";
	MECH_COTF_ON = "I'll start colouring on the fly when your resources change next.";
	MECH_COTF_ALREADY_OFF = "I'm already not colouring on the fly.";
	MECH_COTF_OFF = "I'll stop colouring on the fly when your resources change next.";
	MECH_FOZ_ALREADY_ON = "I'm already fading, did you mean off?";
	MECH_FOZ_ON = "I will now fade the resource when it's zero.";
	MECH_FOZ_ALREADY_OFF = "I'm already not fading, did you mean on?";
	MECH_FOZ_OFF = "I will stop fading the resource when it's zero.";
	MECH_WSCTI_ALREADY_ON = "WSCT integration is already on.";
	MECH_WSCTI_ON = "WSCT integration enabled.";
	MECH_WSCTI_ALREADY_OFF = "WSCT integration is already off.";
	MECH_WSCTI_OFF = "WSCT integration disabled.";
	MECH_WSCTI_NOT_FOUND = "Couldn't find WSCT, so you cannot turn this on.";

end