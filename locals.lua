--
--	Mech 1.81
--	Localisation code file
--

local gla = SystemData.Settings.Language.active
local mcl = SystemData.Settings.Language
mechGameLang = 0

if ( gla == mcl.ENGLISH ) then 
	mechGameLang = "enGB"
elseif ( gla == mcl.FRENCH ) then 
	mechGameLang = "frFR"
elseif ( gla == mcl.GERMAN ) then 
	mechGameLang = "deDE"
elseif ( gla == mcl.ITALIAN ) then 
	mechGameLang = "itIT"
elseif ( gla == mcl.RUSSIAN ) then 
	mechGameLang = "ruRU"
elseif ( gla == mcl.KOREAN ) then 
	mechGameLang = "koKR"
elseif ( gla == mcl.JAPANESE ) then 
	mechGameLang = "jpJP"
elseif ( gla == mcl.T_CHINESE ) then
	mechGameLang = "zhTW"
elseif ( gla == mcl.S_CHINESE ) then 
	mechGameLang = "zhCN"
end

local function pm(msg)
	if ( type(msg) ~= "string" or not msg ) then 
		return;
	else
		EA_ChatWindow.Print(towstring("[Mech] "..msg))
	end
end

if ( ( not mechGameLang ) or ( type(mechGameLang) ~= "string" ) ) then
	pm("I couldn't load my locals!")
	return
end

if ( ( mechGameLang ~= "enGB" ) and ( mechGameLang ~= "frFR" ) and ( mechGameLang ~= "deDE" ) ) then
	pm("Sorry! I don't know your language, so I'm having to use English.")
	mechGameLang = "enGB"
	return
end

mechGameLang = "enGB"
