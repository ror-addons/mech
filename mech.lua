--
--	Mech 1.83
--	Core addon file
--

local pc = GameData.Player.career.line
local gc = GameData.CareerLine

mech = {}
mech.default = 
{ 
	state = "on", 
	version = 1.83, 
	font = "font_clear_large_bold", 
	wscti = "on",
	foz = "off",
	cotf = "on",
	colour1 =
	{ 
		r = 255, 
		g = 255, 
		b = 255
	},
	colour2 =
	{ 
		r = 255,
		g = 255,
		b = 255
	}
}

mech.sarr =
{
	[0] = "Sound placeholder",
}

mech.tarr =
{
	[0] = "Texture placeholder",
}

local function pm(msg)
	if ( type(msg) ~= "string" or not msg ) then 
		return;
	else
		EA_ChatWindow.Print(towstring("[Mech]: "..msg))
	end
end

local function slashcmds()
	pm("Mech")
	pm("--------------------")
	pm("/mech [on|off]")
	pm("/mech [reset]")
	pm("/mech [colour1|colour2] [0-255], [0-255], [0-255]")
	pm("/mech font [1-14]")
	pm("/mech wscti [on|off]")
	pm("/mech foz [on|off]")
	pm("/mech cotf [on|off]")
end

function mech.init()
	CreateWindow("mechwin", true)
	LayoutEditor.RegisterWindow( "mechwin", L"Mech", L"~", false, false, true, nil )
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RESOURCE_UPDATED, "mech.update")
	RegisterEventHandler(SystemData.Events.ENTER_WORLD, "mech.forceupdate")

	if ( not mechs or ( mechs["version"] > mech.default["version"] ) ) then
		mech.css()
	end

	if ( ( type(mechs["version"]) == "string" ) or ( mechs["version"] < mech.default["version"] ) ) then
		mechs["version"] = mech.default["version"]
		for k,v in pairs ( mech.default ) do
			if ( not mechs[k] ) then
				mechs[k] = v
			end
		end
	end

	if ( LibSlash ) then 
		LibSlash.RegisterSlashCmd("mech", mech.handler)
	end

	mech.forceupdate() -- actually calling this twice on init, but this one is for /reloadui

	if ( GetCareerResource(GameData.BuffTargetType.SELF) == 0 ) then
		mech.slc(100,100,100)
	else
		mech.slc(mechs.colour1.r, mechs.colour1.g, mechs.colour1.b)
	end

	mech.slf(mechs.font)
	pm(mechs["version"].." "..MECH_WELCOME_MSG)
end

function mech.css()
	mechs = {}
	for k, v in pairs(mech.default) do
		mechs[k] = v
	end

	mech.reloadcolours()
	mech.slc(100,100,100)
	mech.slf(mechs.font)
end

function mech.reloadcolours()
	if ( pc == gc.SHAMAN ) then
		mechs.colour1 = { r = 200, g = 0, b = 0 } -- red
		mechs.colour2 = { r = 200, g = 200, b = 0 } -- gold
	elseif ( pc == gc.ARCHMAGE ) then
		mechs.colour1 = { r = 0, g = 180, b = 255 } -- blue
		mechs.colour2 = { r = 240, g = 240, b = 0 } -- yellow
	elseif ( pc == gc.BRIGHT_WIZARD ) then
		mechs.colour1 = { r = 255, g = 160, b = 0 } -- orange
		mechs.colour2 = { r = 255, g = 255, b = 255 } -- white (unused)
	elseif ( pc == gc.SORCERER ) then
		mechs.colour1 = { r = 200, g = 0, b = 200 } -- purple
		mechs.colour2 = { r = 255, g = 255, b = 255 } -- white (unused)
	elseif ( pc == gc.WARRIOR_PRIEST ) then
		mechs.colour1 = { r = 0, g = 180, b = 255 } -- blue
		mechs.colour2 = { r = 255, g = 255, b = 255 } -- white (unused)
	elseif ( pc == gc.BLOOD_PRIEST ) then
		-- dok
		mechs.colour1 = { r = 200, g = 0, b = 200 } -- purple
		mechs.colour2 = { r = 255, g = 255, b = 255 } -- white (unused)
	elseif ( pc == gc.BLACK_ORC ) then
		mechs.colour1 = { r = 255, g = 255, b = 0 } -- yellow
		mechs.colour2 = { r = 224, g = 0, b = 0 } -- red
	elseif ( pc == gc.IRON_BREAKER ) then
		mechs.colour1 = { r = 255, g = 255, b = 0 } -- yellow
		mechs.colour2 = { r = 255, g = 255, b = 255 } -- white (unused)
	elseif ( pc == gc.SWORDMASTER ) then
		mechs.colour1 = { r = 50, g = 160, b = 110 } -- greenish
		mechs.colour2 = { r = 240, g = 200, b = 5 } -- yellowish
	elseif ( pc == gc.ASSASSIN ) then
		-- witch elf
		mechs.colour1 = { r = 200, g = 0, b = 200 } -- purple
		mechs.colour2 = { r = 255, g = 255, b = 255 } -- white (unused)
	elseif ( pc == gc.WITCH_HUNTER ) then
		mechs.colour1 = { r = 70, g = 150, b = 0 } -- dark green
		mechs.colour2 = { r = 255, g = 255, b = 255 } -- white (unused)
	else
		-- default to white
		mechs.colour1 = { r = 255, g = 255, b = 255 }
		mechs.colour2 = { r = 255, g = 255, b = 255 }
	end
end

function mech.shutdown()
	UnregisterEventHandler(SystemData.Events.PLAYER_CAREER_RESOURCE_UPDATED, "mech.update")
	UnregisterEventHandler(SystemData.Events.ENTER_WORLD, "mech.forceupdate")
	if ( LibSlash ) then 
		LibSlash.UnregisterSlashCmd("mech")
	end
end

function mech.slc(red,green,blue)
	if ( not red or not green or not blue ) then
		return;
	else
		LabelSetTextColor("mechwinText", red, green, blue)
	end
end

function mech.slf(font)
	if ( not font ) then
		return
	else
		LabelSetFont("mechwinText", font, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
		mechs["font"] = font
	end
end

function mech.slt(text)
	if ( not text ) then
		return
	else
		LabelSetText("mechwinText", L""..towstring(text))
	end
end

function mech.forceupdate()
	pc = GameData.Player.career.line
	gc = GameData.CareerLine
	mech.update(CareerResourceData:GetPrevious(),GetCareerResource(GameData.BuffTargetType.SELF))
end

function mech.update(prv, crv)
	if ( crv == 0 and mechs["foz"] == "on" ) then
		mech.slt(" ")
		return
	else
		if ( mechs["state"] == "on" ) then
			if ( mechs["cotf"] == "on" and crv ~= 0 ) then 
				mech.reloadcolours()
			end

			if ( pc == gc.SHAMAN ) or ( pc == gc.ARCHMAGE ) then
				if ( crv > 5 ) then
					mech.slt(crv-5)
					mech.slc(mechs.colour1.r, mechs.colour1.g, mechs.colour1.b)
				elseif ( crv < 6 ) then
					if ( crv ==  0 ) then
						if ( mechs["foz"] == "off" ) then
							mech.slt("0")
							mech.slc(100,100,100)
						else
							mech.slt(" ")
						end
					else
						mech.slt(crv)
						mech.slc(mechs.colour2.r, mechs.colour2.g, mechs.colour2.b)
					end
				end
			elseif ( pc == gc.BLACK_ORC ) then
				if ( crv == 1 ) then
					mech.slt("da' Gud Plan!")
					mech.slc(mechs.colour1.r, mechs.colour1.g, mechs.colour1.b)
				elseif ( crv == 2 ) then
					mech.slt("da' Best Plan!!")
					mech.slc(mechs.colour2.r, mechs.colour2.g, mechs.colour2.b)
				else
					if ( mechs["foz"] == "off" ) then
						mech.slt("No Plan")
						mech.slc(100,100,100)
					else
						mech.slt(" ")
					end
				end
			elseif ( pc == gc.SWORDMASTER ) then
				if ( crv == 1 ) then
					mech.slt("Improved Balance")
					mech.slc(mechs.colour1.r, mechs.colour1.g, mechs.colour1.b)
				elseif ( crv == 2 ) then
					mech.slt("Perfect Balance")
					mech.slc(mechs.colour2.r, mechs.colour2.g, mechs.colour2.b)
				else
					if ( mechs["foz"] == "off" ) then
						mech.slt("Balance")
						mech.slc(100,100,100)
					else
						mech.slt(" ")
					end
				end
			else
				if ( crv == 0 ) then
					if ( mechs["foz"] == "off" ) then
						mech.slt(crv)
						mech.slc(100,100,100)
					else
						mech.slt(" ")
					end
				else
					mech.slt(crv)
					mech.slc(mechs.colour1.r, mechs.colour1.g, mechs.colour1.b)
				end
			end

			if ( WSCT and mechs["wscti"] == "on" and crv ~= 0) then
				
				if ( not stopspam ) then
					stopspam = 0
				else
					if ( stopspam == crv ) then
						return
					end
				end

				if ( pc == gc.SHAMAN ) then 
					if ( crv > 5 ) then 
						crt = "Mork's Waaagh!"	
						stopspam = crv
						WSCT:Display_Event("SHOWBUFF", L"["..towstring((crv-5).." "..crt)..L"]")
						return
					else
						crt = "Gork's Waaagh!"
					end
				elseif ( pc == gc.BRIGHT_WIZARD ) then 
					crt = "Combustion"
				elseif ( pc == gc.SORCERER ) then
					crt = "Dark Magic"
				elseif ( pc == gc.ARCHMAGE ) then
					if ( crv > 5 ) then 
						crt = "Tranquility" 
						stopspam = crv
						WSCT:Display_Event("SHOWBUFF", L"["..towstring((crv-5).." "..crt)..L"]")
						return
					else
						crt = "Force"
					end
				elseif ( pc == gc.WARRIOR_PRIEST ) then
					crt = "Righteous Fury"
				elseif ( pc == gc.BLOOD_PRIEST ) then
					-- dok
					crt = "Righteous Fury"
				elseif ( pc == gc.BLACK_ORC ) then
					if ( crv == 1 ) then
						crt = "da' Gud Plan!"
					else
						crt = "da' Best Plan!!"
					end

					stopspam = crv
					WSCT:Display_Event("SHOWBUFF", L"["..towstring(crt)..L"]")
					return
				elseif ( pc == gc.SWORDMASTER ) then
					if ( crv == 1 ) then
						crt = "Improved Balance"
					else
						crt = "Perfect Balance"
					end

					stopspam = crv
					WSCT:Display_Event("SHOWBUFF", L"["..towstring(crt)..L"]")
					return
				elseif ( pc == gc.IRON_BREAKER ) then
					crt = "Grudges"
				elseif ( pc == gc.ASSASSIN ) then
					-- witch elf
					crt = "Blood Lust"
				elseif ( pc == gc.WITCH_HUNTER ) then
					if ( crv == 1 ) then
						crt = "Accusation"
					else
						crt = "Accusations"
					end
				end

				if ( not crt ) then
					crt = "undefined"
				end

				stopspam = crv
				WSCT:Display_Event("SHOWBUFF", L"["..towstring(crv.." "..crt)..L"]")
				return
			end
		else 
			mech.slt(" ")
		end
	end
end

function mech.handler(cmd)
	local opt, val = cmd:match("(%w+)[ ]?(.*)")
	if ( not opt or not val ) then
		slashcmds()
	else
		if ( opt == "on" or opt == "1" ) then
			if ( mechs["state"] == "on" ) then
				pm(MECH_STATE_ALREADY_ON)
			else
				mechs["state"] = "on"
				pm(MECH_STATE_ON)
				mech.forceupdate()
			end
		elseif ( opt == "off" or opt == "0" ) then
			if ( mechs["state"] == "off" ) then
				pm(MECH_STATE_ALREADY_OFF)
			else
				mechs["state"] = "off"
				pm(MECH_STATE_OFF)
				mech.forceupdate()
			end
		elseif ( opt == "colour" or opt == "color" ) then
			pm("/mech [colour1|colour2] [0-255], [0-255], [0-255]")
		elseif ( opt == "colour1" or opt == "colour2" ) then
			if ( mechs["cotf"] == "on" ) then
				pm(MECH_COLOURS_COTF_ON)
				return
			end
			_, _, red, green, blue = string.find(val, "(%d+),(%d+),(%d+)")
			if ( red and green and blue ) then
				red = tonumber(red)
				green = tonumber(green)
				blue = tonumber(blue)
				if ( type(red) ~= "number" and type(green) ~= "number" and type(blue) ~= "number" ) then
					pm(MECH_COLOURS_COLOUR_INVALID)
					pm("/mech "..opt.." [0-255],[0-255],[0-255]")
				else
					if ( red < 0 or red > 255 or green < 0 or green > 255 or blue < 0 or blue > 255) then
						pm(MECH_COLOURS_NUMBER_INVALID)
						pm("/mech "..opt.." [0-255],[0-255],[0-255]")
					else
						-- pm("Colours #"..opt:sub(string.len(opt)).." changed.")
						pm (MECH_COLOUR_DONE)
						if ( opt == "colour1" ) then
							mechs.colour1 = { r = red, g = green, b = blue }
							if ( GetCareerResource(GameData.BuffTargetType.SELF) ~= 0 ) then
								mech.slc(mechs.colour1.r, mechs.colour1.g, mechs.colour1.b)
							end
						elseif ( opt == "colour2" ) then
							mechs.colour2 = { r = red, g = green, b = blue }
							if ( GetCareerResource(GameData.BuffTargetType.SELF) ~= 0 ) then
								mech.slc(mechs.colour2.r, mechs.colour2.g, mechs.colour2.b)
							end
						end
					end
				end
			else
				pm(MECH_COLOUR_NEED_THREE)
				pm("/mech "..opt.." [0-255],[0-255],[0-255]")
			end
		elseif ( opt == "reset" ) then
			pm(MECH_OPTIONS_RESET)
			mech.css()
		elseif ( opt == "font")  then
			val = tonumber(val)
			if ( type(val) == "number" ) then
				       if ( val == 1 ) then mech.slf("font_chat_text")
				elseif ( val == 2 )  then mech.slf("font_clear_small")
				elseif ( val == 3 ) then mech.slf("font_clear_medium")
				elseif ( val == 4 ) then mech.slf("font_clear_large")
				elseif ( val == 5 ) then mech.slf("font_clear_small_bold")
				elseif ( val == 6 ) then mech.slf("font_clear_medium_bold")
				elseif ( val == 7 ) then mech.slf("font_clear_large_bold")
				elseif ( val == 8 ) then mech.slf("font_default_text_small")
				elseif ( val == 9 ) then mech.slf("font_default_text")
				elseif ( val == 10 ) then mech.slf("font_default_text_large")
				elseif ( val == 11 ) then mech.slf("font_default_text_huge")
				elseif ( val == 12 ) then mech.slf("font_heading_large")
				elseif ( val == 13 ) then mech.slf("font_heading_huge")
				elseif ( val == 14 ) then mech.slf("font_journal_text")
				else
					pm(MECH_FONT_INVALID)
					pm("/mech font [1-14]")
				end

				if ( val >= 1 and val <= 14 ) then
					pm(MECH_DONE)
				end
			else
				pm(MECH_DONT_UNDERSTAND)
				pm("/mech font [1-14]")
			end
		elseif ( opt == "cotf" ) then
			if ( val == "on" ) then
				if ( mechs["cotf"] == "on" ) then
					pm(MECH_COTF_ALREADY_ON)
				else
					mechs["cotf"] = "on"
					pm(MECH_COTF_ON)
					mech.reloadcolours()
				end
			elseif ( val == "off" ) then
				if ( mechs["cotf"] == "off" ) then
					pm(MECH_COTF_ALREADY_OFF)
				else
					mechs["cotf"] = "off"
					pm(MECH_COTF_OFF)
					mech.reloadcolours()				
				end
			else
				pm(MECH_DONT_UNDERSTAND)
				pm("/mech cotf [on|off]")
			end 
		elseif ( opt == "foz" ) then
			if ( val == "on" ) then
				if ( mechs["foz"] == "on" ) then
					pm(MECH_FOZ_ALREADY_ON)
				else
					mechs["foz"] = "on"
					pm(MECH_FOZ_ON)
					mech.forceupdate()
				end
			elseif ( val == "off" ) then
				if ( mechs["foz"] == "off" ) then
					pm(MECH_FOZ_ALREADY_OFF)
				else
					mechs["foz"] = "off"
					pm(MECH_FOZ_OFF)
					mech.forceupdate()					
				end
			else
				pm(MECH_DONT_UNDERSTAND)
				pm("/mech foz [on|off]")
			end 
		elseif ( opt == "wscti" ) then
			if ( WSCT ) then
				if ( val == "on" ) then
					if ( mechs["wscti"] == "on" ) then
						pm(MECH_WSCTI_ALREADY_ON)
					else
						mechs["wscti"] = "on"
						pm(MECH_WSCTI_ON)
					end
				elseif ( val == "off" ) then
					if ( mechs["wscti"] == "off" ) then
						pm(MECH_WSCTI_ALREADY_OFF)
					else
						mechs["wscti"] = "off"
						pm(MECH_WSCTI_OFF)
					end
				else
					pm(MECH_DONT_UNDERSTAND)
					pm("/mech wscti [on|off]")
				end
			else
				pm(MECH_WSCTI_NOT_FOUND)
			end
		else
			slashcmds()
		end
	end        
end
